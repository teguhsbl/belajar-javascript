//TODO SWITCH CASE
// SPECIAL CONDITION
// digunakan ketika hanya menentukan 1 kondisi
/* TODO SYNTAX SWITCH
  switch (expression) {
    case 'case1':
      // code
      break;
    case 'case2':
      break;
    default:
      // code
      break;
  }
  TODO CARA KERJA SWITCH
  * expression switch dievaluasi sekali.
  * Nilai ekspresi dibandingkan dengan nilai setiap kasus.
  * Jika ada kecocokan, blok kode yang terkait akan dieksekusi.
  * Jika tidak ada kecocokan, blok kode default dijalankan
*/

//FIXME CASE I
let password = 'teguh123';
let ifOutput;
let switchOutput;
//IN IF 
if (password == 'teguh123') {
  ifOutput = 'login berhasil';
} else {
  ifOutput = 'login gagal';
}
console.log(ifOutput)

switch (password) {
  case 'teguh123':
    switchOutput = 'login berhasil';
    break;
  default:
    switchOutput = 'login gagal';
    break;
}
console.log(switchOutput)