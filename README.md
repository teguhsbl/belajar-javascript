# Belajar Javascript

Repo belajar Javascript dari nol
```js
console.log('Selamat Belajar')
```

## Clone Repo Untuk Belajar

Silahkan clone repo ini untuk bahan belajar kamu dengan cara

```
git clone https://gitlab.com/teguhsbl/belajar-javascript.git
```

## Catatan
Kode di repo ini bersifat gratis untuk dipelajari oleh siapa saja, tidak untuk di jual

## Follow My Sosial :
[facebook](fb.me/teguhs11.owner), [instagram](instagram.com/teguhs11_), [github](github.com/teguhtech), [gitlab](gitlab.com/teguhsbl)