// 3 TYPE VARIABLE

/* TODO Type data Variable
1. VAR > Dapat Diubah
2. LET > Dapat Diubah (Lebih Sering di Pake)
3. CONST > Tidak Dapat Diubah
*/

//TODO TYPE DATA
/*
1. Primitif
2. Reference
*/

// Primitif
/* 
1. Boolean >> True, false
2. Number >> 1,2,3...
3. String >> "Melati", "Mawar", "Anggrek"
4. Null >> NULL adalah suatu nilai yang tidak bernilai sama sekali. Suatu kolom yang sifatnya optional dapat mempunyai nilai NULL. Kolom optional artinya datanya bisa diisi bisa tidak. Apabila tidak diisi maka otomatis nilainya NULL.
5. Undefined >> Undefined berarti sebuah variabel belum dideklarasikan, atau telah dinyatakan namun belum diberi nilai.
*/

let x = 5; // Menetapkan nilai 5 ke variable x
let y = 2; // Menetapkan nilai 2 ke Variable y
let z = x + y; // Menetapkan nilai variable x + y (5 + 2) ke variable z
console.log(z);
// TODO Operator Aritmatika
/*
+ Tambah
- Kurang
* Kali
/ Bagi
** Pangkat
% Modulus (Mencari sisa bagi)
++ Increment (Auto Tambah 1)
-- Decrement (Auto Kurang 1)
+= (b+=y) : b = b + y 
-= (b-=) : b = b - y
TODO Operator Perbandingan
> lebih dari 
< Kurang dari
>= lebih besar sama dengan
<= lebih kecil sama dengan
== sama dengan
!= tidak sama dengan
*/