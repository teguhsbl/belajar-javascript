//TODO TRULY AND FALSY CONCEPT
// Boolean
let a = true; //TRULY
let b = false; // FALSY

// Number
let c = 1; //... -1, !=0, 1, 2, 3, .. TRULY
let d = 0; // FALSY

// String 
let e = 'budi'; //TRULY
let f = ''; //FALSY

undefined, NaN, null; // FALSY

// Converting value => !
if (!f) {
  console.log('Kode berjalan')
}