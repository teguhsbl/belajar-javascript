//TODO Nested If (If bercabang)
let hewan1 = 'buaya';
let makanan = 'daging';
let golongan;
if (hewan1 == 'buaya') {
  if (makanan == 'daging') {
    golongan = 'karnivora';
  }
}
console.log(`hewan ${hewan1} adalah pemakan ${makanan} dan termasuk golongan hewan ${golongan}`);

//FIXME CASE I 
const umur = 18; //number
let tahap;
if (umur <= 18) { // jika umur dibawah sama dengan 18
  if (umur <= 5) { // dan jika umur dibawah sama dengan 5
    tahap = 'balita';
  } else {
    tahap = 'anak-anak'; // jika umur di dibawah sama dengan 18 dan tidak sama dengan di bawah sama dengan 5 
  }
} else if (umur > 18) { // jika umur di atas 18
  if (umur <= 50) { // dan jika umur di bawah sama dengan 50
    tahap = 'dewasa';
  } else { // jika umur di atas 18 dan tidak sama dengan di bawah 50
    tahap = 'lansia';
  }
}
console.log(`Umur : ${umur} adalah ${tahap}`);

//FIXME CASE II 
let ukuranBaju = 'L' // M / L
let merek = 'Guci' // Guci / Prada / IMP
qty = 2;
let harga;
let totalHarga;
if (ukuranBaju == 'M') {
  if (merek == 'Guci') {
    harga = 6000;
  } else if (merek == 'Prada') {
    harga = 10000;
  } else if (merek == 'IMP') {
    harga = 15000;
  }
} else if (ukuranBaju == 'L') {
  if (merek == 'Guci') {
    harga = 7000;
  } else if (merek == 'Prada') {
    harga = 11000;
  } else if (merek == 'IMP') {
    harga = 16000;
  } 
}
if (harga) {
  totalHarga = harga * qty;
} else {
  totalHarga = 'tidak valid'
}

console.log(`Ukuran baju ${ukuranBaju} dengan merek ${merek} sebanyak ${qty} total harganya ${totalHarga}`)
