// TODO intermediate
let a = "Melati";
let b;
// console.log(a);
if (a == "Melati") {
  b = 'bunga';
}
console.log(`${a} adalah jenis ${b}`)

//TODO 
/*
if (kondisi1) {
  // Jika kondisi1 true maka blok kode ini akan di eksekusi
} else if (kondisi2) {
  // Jika kondisi1 tidak true maka akan di jalankan kondisi2 dan jika kondisi2 true blok kode ini akan di eksekusi
} else {
  // jika kedua kondisi tidak true maka blok kode ini yang akan di eksekusi
}
*/

//FIXME CASE I 
let nilai1 = 10;
let nilai2 = 3;
let hasil;
if (( nilai1 + nilai2 ) % 2 == 0) {
  hasil = 'Ini bilangan genap';
} else if (( nilai1 + nilai2 ) % 2 == 1) {
  hasil = 'Ini bilangan ganjil';
} else {
  hasil = 'Nilai tidak valid';
}
console.log(hasil);

//FIXME CASE II 
let gender = 'male';
pesanError = 'Gender tidak ditemukan';

if (gender == 'male') {
  console.log('Nama saya Budi');
} else if (gender == 'female') {
  console.log('Nama saya Tami');
} else {
  console.log(pesanError);
}